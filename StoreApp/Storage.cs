﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public class Storage : IStorage
	{
		private readonly string m_name;
		private Dictionary<Type, object> m_repositories;
		public string Name => m_name;

		public Storage(string name)
		{
			m_name = name;
			m_repositories = new Dictionary<Type, object>();
		}

		public void AddRepository<TProduct>(IProductsRepository<TProduct> productRepository)
		{
			if (!m_repositories.ContainsKey(typeof(IProductsRepository<TProduct>)))
			{
				m_repositories[typeof(IProductsRepository<TProduct>)] = productRepository;
			}
		}

		public void RemoveRepository<TProduct>(IProductsRepository<TProduct> productRepository)
		{
			if (m_repositories.ContainsKey(typeof(IProductsRepository<TProduct>)))
			{
				m_repositories.Remove(typeof(IProductsRepository<TProduct>));
			}
		}

		public IProductsRepository<TProduct> GetRepository<TProduct>()
		{
			m_repositories.TryGetValue(typeof(IProductsRepository<TProduct>), out object repository);
			return repository as IProductsRepository<TProduct>;
		}
	}
}
