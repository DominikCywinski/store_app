﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public interface IProductsRepository<T>
	{
		void AddProduct(T product);
		void RemoveProduct(T product);
		T GetProductById(string id);
		List<T> GetProducts();
	}
}
