﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public interface IStorage
	{
		string Name { get; }
		void AddRepository<TProduct>(IProductsRepository<TProduct> productRepository);
		void RemoveRepository<TProduct>(IProductsRepository<TProduct> productRepository);
		IProductsRepository<TProduct> GetRepository<TProduct>();
	}
}
