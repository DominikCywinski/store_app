﻿using System;

namespace StoreApp
{
	class Program
	{
		static void Main(string[] args)
		{
			var storage = new Storage("BMX");

			var bikes = new ProductsRepository<Bike>();
			var helmets = new ProductsRepository<Helmet>();

			storage.AddRepository(bikes);
			storage.AddRepository(helmets);

			for (int i = 0; i < 100; i++)
			{
				bikes.AddProduct(new Bike(i.ToString(), "BMX", i, 3 * i - 2, 3 * i, "red"));
			}

			for (int i = 0; i < 100; i++)
			{
				helmets.AddProduct(new Helmet(i.ToString(), "Helmet1", 2 * i, 3 * i, 3 * i, "blue","medium"));
			}

			foreach (var item in storage.GetRepository<Bike>().GetProducts())
			{
				Console.WriteLine($"Bikes Id: {item.Id} Price: {item.Price}");
			}

			foreach (var item in storage.GetRepository<Helmet>().GetProducts())
			{
				Console.WriteLine($"Helmets Id: {item.Id} Price: {item.Price}");
			}

			Console.WriteLine(storage.GetRepository<Bike>());
			Console.WriteLine(storage.GetRepository<Helmet>());
			Console.WriteLine(storage.GetRepository<Tire>());
		}
	}
}
