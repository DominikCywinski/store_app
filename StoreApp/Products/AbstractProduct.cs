﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public abstract class AbstractProduct : IProduct
	{
		private string m_id;
		private string m_name;
		private decimal m_price;
		private string m_stand;
		private decimal m_shelf;
		private Coordinates m_coordinates;

		public string Id => m_id;
		public string Name => m_name;
		public decimal Price => m_price;
		public Coordinates Coordinates => m_coordinates;

		public AbstractProduct(string id, string name, decimal price, int stand, int shelf)
		{
			m_id = id;
			m_name = name;
			m_price = price;
			m_coordinates = new Coordinates { Stand = stand, Shelf = shelf };
		}
	}
}
