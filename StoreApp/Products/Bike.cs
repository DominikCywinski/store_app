﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public class Bike : AbstractProduct
	{
		private string m_color;

		public Bike(string id, string name, decimal price, int stand, int shelf, string color) : base(id, name, price, stand, shelf)
		{
			m_color = color;
		}
	}
}
