﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public interface IProduct
	{
		string Id { get; }
		string Name { get; }
		decimal Price { get; }
		Coordinates Coordinates { get; }
	}
}
