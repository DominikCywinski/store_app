﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public class Tire : AbstractProduct
	{
		private string m_size;

		public Tire(string id, string name, decimal price, int stand, int shelf, string size) : base(id, name, price, stand, shelf)
		{
			m_size = size;
		}
	}
}
