﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public class Helmet : AbstractProduct
	{
		private string m_color;
		private string m_size;

		public Helmet(string id, string name, decimal price, int stand, int shelf, string color, string size) : base(id, name, price, stand, shelf)
		{
			m_color = color;
			m_size = size;
		}
	}
}
