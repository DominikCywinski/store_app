﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public struct Coordinates
	{
		public int Shelf;
		public int Stand;
	}
}
