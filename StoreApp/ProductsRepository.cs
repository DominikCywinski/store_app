﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreApp
{
	public class ProductsRepository<T> : IProductsRepository<T> where T : class, IProduct
	{
		private List<T> m_product = new List<T>();

		public void AddProduct(T product)
		{
			if (GetProductById(product.Id) == null)
			{
				m_product.Add(product);
			}
		}

		public void RemoveProduct(T product)
		{
			m_product.Remove(product);
		}

		public T GetProductById(string id)
		{
			foreach (var product in m_product)
			{
				if (id == product.Id)
				{
					return product;
				}
			}

			return null;
		}

		public List<T> GetProducts()
		{
			return m_product;
		}
	}
}
